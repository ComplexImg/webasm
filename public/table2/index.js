
function jsMakeTable() {
  // table section
  var tbl = new WebAssembly.Table({initial:2, element:"anyfunc"});

  // function sections:
  var f1 = function() { }
  var f2 = function() { }

  // elem section
  tbl.set(0, f1);
  tbl.set(1, f2);
};


var otherTable = new WebAssembly.Table({ element: "anyfunc", initial: 2 });

WebAssembly.instantiateStreaming(fetch('public/table2/wasm-table.wasm'))
  .then(({instance}) => {

    console.log(instance.exports.callByIndex(0)); // returns 42
    console.log(instance.exports.callByIndex(1)); // returns 13
    console.log(instance.exports.callByIndex(2)); // returns an error, because there is no index position 2 in the table
  });
  
  