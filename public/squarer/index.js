let squarer;

// variant 1 --------------

// function loadWebAssembly(fileName) {
//   return fetch(fileName)
//     .then(response => response.arrayBuffer())
//     .then(bits => WebAssembly.compile(bits))
//     .then(module => { return new WebAssembly.Instance(module) });
// };
  

// loadWebAssembly('wasm/squarer.wasm')
//   .then(instance => {
//     squarer = instance.exports._Z7squareri;
//     console.log('Finished compiling! Ready when you are...');

//     console.log('result', squarer(9));
//   });


// variant 2 --------------

fetch('public/squarer/squarer.wasm')
  .then(response => response.arrayBuffer())
  .then(bits => WebAssembly.compile(bits))
  .then(module => { return new WebAssembly.Instance(module) })
  .then(instance => {
    squarer = instance.exports._Z7squareri;
    console.log('Finished compiling! Ready when you are...');
    console.log('result', squarer(9));
  });


// variant 3 --------------

// WebAssembly.instantiateStreaming(fetch('public/squarer/squarer.wasm'), {})
//   .then(({instance, module}) => {
//     squarer = instance.exports._Z7squareri;
//     console.log('Finished compiling! Ready when you are...');

//     console.log('instance', instance);
//     console.log('module', module);

//     console.log('result', squarer(9));
//   });


