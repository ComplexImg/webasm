var importObject = {
  imports: { imported_func: arg => console.log(arg) }
};

// WebAssembly.instantiateStreaming(fetch('public/simple/simple.wasm'), importObject)
//   .then(({instance}) => {
//     const exported_func = instance.exports.exported_func;
//     console.log('Finished compiling! Ready when you are...');

//     console.log('instance', instance);

//     exported_func();
//   });


  WebAssembly.compileStreaming(fetch('public/simple/simple.wasm'))
  .then(module => WebAssembly.instantiate(module, importObject))
  .then((instance) => {
  
    const exported_func = instance.exports.exported_func;
    console.log('Finished compiling! Ready when you are...');
  
    console.log('instance', instance);
  
    exported_func();
  });
  
  