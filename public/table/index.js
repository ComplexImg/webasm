var otherTable = new WebAssembly.Table({ element: "anyfunc", initial: 2 });

WebAssembly.instantiateStreaming(fetch('public/table/table.wasm'))
  .then((results) => {

    var tbl = results.instance.exports.tbl;
    console.log(tbl.get(0)());  // 13
    console.log(tbl.get(1)());  // 42
    
    otherTable.set(0,tbl.get(0));
    otherTable.set(1,tbl.get(1));
    
    console.log(otherTable.get(0)());
    console.log(otherTable.get(1)());

  });
  
  