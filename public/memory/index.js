const memory = new WebAssembly.Memory({initial:10, maximum:100});

new Uint32Array(memory.buffer)[0] = 42;

console.log(new Uint32Array(memory.buffer)[0])

memory.grow(69);

WebAssembly.instantiateStreaming(fetch('public/memory/memory.wasm'), { js: { mem: memory } })
  .then(({instance}) => {
   
    console.log(new Uint32Array(memory.buffer)[0]);

    var i32 = new Uint32Array(memory.buffer);

    for (var i = 0; i < 10; i++) {
      i32[i] = i;
    }

    var sum = instance.exports.accumulate(0, 10);
    console.log(sum);
   

  });


