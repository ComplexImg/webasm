const memory = new WebAssembly.Memory({initial:1});

const global = new WebAssembly.Global({value:'i32', mutable:false}, 2);

var importObject = {
  imports: {
     printInt: arg => console.log("print from wasm: sum / " + global + " = " + arg),
     memory,
     global,
    }
};

WebAssembly.instantiateStreaming(fetch('public/example/example.wasm'), importObject)
  .then(({instance}) => {

    var i32 = new Uint32Array(memory.buffer);

    console.log('memory start state: ' + i32);

    for (var i = 0; i < 10; i++) {
      i32[i] = i;
    }

    console.log('memory after init: ' + new Uint32Array(memory.buffer));

    // use export func for calc and get result
    var sum = instance.exports.accumulate(0, 10);

    // print result from js
    console.log(sum);
  });


