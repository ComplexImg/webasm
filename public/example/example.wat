(module
  (type $PrintIntType (func (param i32)))
  (type $AccumulateType (func (param i32 i32) (result i32)))
  (import "imports" "memory" (memory $imports.memory 1))
  (import "imports" "printInt" (func $imports.printInt (type $PrintIntType)))
  (import "imports" "global" (global $imports.div (i32)))
  (func $accumulate (type $AccumulateType) (param $from i32) (param $to i32) (result i32)
    (local $maxValue i32) (local $sum i32)
    get_local $from
    get_local $to
    i32.const 4                            ;; js int = 4 byte => max value in loop = 40
    i32.mul
    i32.add
    set_local $maxValue                    ;; 40 - max loop value 
    block $B0
      loop $Loop
        get_local $from
        get_local $maxValue
        i32.eq                             ;; if !== then go next line
        br_if $B0
        get_local $sum
        get_local $from
        i32.load
        i32.add
        set_local $sum
        get_local $from
        i32.const 4                         ;; index += 4
        i32.add
        set_local $from
        br $Loop
      end
    end
    get_local $sum
    get_global $imports.div
    i32.div_s                              ;; sum / global
    call $imports.printInt
    get_local $sum
    )
  (export "accumulate" (func $accumulate)))
