var express = require('express');

var app = express();

// http://localhost:8080/
app.get('/', function(req, res) {
  res.sendfile('index.html');
});

app.use("/public", express.static(__dirname + "/public"));

// port 8080
app.listen(8080);

console.log('Server started!');