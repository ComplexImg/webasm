Most of the examples were taken from the mdn repository: [https://github.com/mdn/webassembly-examples/](https://github.com/mdn/webassembly-examples/)

There are some examples with comments and some from me. 

Also here is a built-in server, thanks to which you can run and add new examples without unnecessary movements.

For start:
- Use command `yarn`.
- Then use `yarn start`.
- Open in browser 'localhost:8080' 